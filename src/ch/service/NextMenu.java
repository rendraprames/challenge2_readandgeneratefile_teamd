package ch.service;

import java.util.Scanner;

/**
 * Implementasi Inhearitance dari MainMenuAbstract Class
 */
public class NextMenu extends MainMenuAbstract{

    /**
     * Implementasi Polymorfing dari method tampil(Overriding)
     */
    @Override
    public void tampil() {
        MainMenu menu = new MainMenu();
        Scanner input = new Scanner(System.in);
        System.out.println("=============================");
        System.out.println("Aplikasi Pengolah Nilai Siswa");
        System.out.println("=============================");
        System.out.println("Pemrosesan telah selesai, silahkan pilih menu");
        System.out.println("Output File telah di generate ke folder yang sama");
        System.out.println("0. Exit");
        System.out.println("1. kembali ke menu utama");
        System.out.println("Silahkan pilih menu");
        int pilih = input.nextInt();
        switch (pilih){
            case 0 : {
                System.out.println("Program SELESAI");
                System.exit(0);
                break;
            }
            case 1 : menu.tampil();
            break;
            default : {
                System.out.println("Pilihan tidak ditemukan, silahkan pilih lagi");
                this.tampil();
            }

        }
    }
}
